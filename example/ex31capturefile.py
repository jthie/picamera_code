from time import sleep
from picamera import PiCamera

# Calling start_preview() opens an oversiezed window that shows the camera view
# but it cannot be closed.
# Hence, pass the window size arguments to make the window smaller.
camera = PiCamera()
camera.resolution = (1024,768)
camera.start_preview(fullscreen=False,window=(500,200,800,800))
sleep(5)
camera.capture('/home/pi/ftp/files/testcapture.jpg')
camera.stop_preview()
