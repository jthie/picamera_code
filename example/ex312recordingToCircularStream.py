import io
import random
import picamera

def motion_detected():
    # Randomly return True (like a fake motion detection routine)
	outcome = random.randint(0,10)
	print(outcome)
	outcome = outcome < 2
	return outcome
    #return random.randint(0,10) == 0

# initialise the picamera
camera = picamera.PiCamera()
stream = picamera.PiCameraCircularIO(camera, seconds=20)
camera.start_recording(stream, format='h264')
try:
    while True:
        camera.wait_recording(1) # Record one second at a time
        if motion_detected():    # Check for motion
            # Keep recording for 10 seconds and only then write the
            # stream to disk
            camera.wait_recording(10)
            stream.copy_to('ex312circularStream.h264') # save to file 
finally:
    camera.stop_recording()
	