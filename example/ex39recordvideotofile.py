import picamera

cam = picamera.PiCamera()
# For list of resolution, refer to the documentation v1.13
# section 6.2 Camera hardware > sensor modes
#cam.resolution = (640,480)
cam.resolution = (1920,1080)
print("Start recording now")
filename = "/home/pi/ftp/files/testvideo" + str(35) + ".h264"
cam.start_recording(filename)
cam.wait_recording(5)
cam.stop_recording()
print("End recording")

