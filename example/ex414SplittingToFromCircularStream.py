import io
import random
import picamera
from PIL import Image
from numpy import *

prior_image = None

def detect_motion(camera):
    global prior_image
    stream = io.BytesIO()
    camera.capture(stream, format='jpeg', use_video_port=True)
    stream.seek(0)
    if prior_image is None:
        prior_image = Image.open(stream)
        return False
    else:
        current_image = Image.open(stream)
        # Compare current_image to prior_image to detect motion. This is
        # left as an exercise for the reader!
        result = random.randint(0, 10) < 2 # for testing only
        print(result)
        # Convert the images to arrays
        currImage = array(current_image.convert('L'))
#         print(currImage.shape) 
#         print(currImage.dtype)
        prevImage = array(prior_image.convert('L'))
#         print(prevImage.shape) 
#         print(prevImage.dtype)
        # Evaluate the difference
        diffImage = sum(absolute(subtract(float32(currImage), float32(prevImage))))/720/1280
#         diffImage = sum(absolute(subtract(float32(currImage), float32(prevImage))))
#         print(diffImage.shape)
#         print(diffImage.dtype)
        print(diffImage)

        # Once motion detection is done, make the prior image the current
        prior_image = current_image
        if diffImage > 30:
            return True
        else:
            return False
        

with picamera.PiCamera() as camera:
    camera.resolution = (1280, 720)
    stream = picamera.PiCameraCircularIO(camera, seconds=10)
    camera.start_recording(stream, format='h264')
    try:
        while True:
            camera.wait_recording(1)
            if detect_motion(camera):
                print('Motion detected!')
                # As soon as we detect motion, split the recording to
                # record the frames "after" motion
                camera.split_recording('after.h264')
                # Write the 10 seconds "before" motion to disk as well
                stream.copy_to('before.h264', seconds=10)
                stream.clear()
                # Wait until motion is no longer detected, then split
                # recording back to the in-memory circular buffer
                camera.wait_recording(2)
                while detect_motion(camera):
                    camera.wait_recording(1)
                print('Motion stopped!')
                break
                camera.split_recording(stream)
    finally:
        camera.stop_recording()