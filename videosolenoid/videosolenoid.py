import io
import random
import picamera
from PIL import Image
from numpy import *

# Import library to get the hour of the day
from datetime import datetime
#import time

# Import GPIO library
from gpiozero import LED


prior_image = None

def detect_motion(camera):
    global prior_image
    stream = io.BytesIO()
    camera.capture(stream, format='jpeg', use_video_port=True)
    stream.seek(0)
    if prior_image is None:
        prior_image = Image.open(stream)
        return False
    else:
        current_image = Image.open(stream)
        # Compare current_image to prior_image to detect motion. This is
        # left as an exercise for the reader!
        result = random.randint(0, 10) < 2 # for testing only
        print(result)
        # Convert the images to arrays
        currImage = array(current_image.convert('L'))
#         print(currImage.shape) 
#         print(currImage.dtype)
        prevImage = array(prior_image.convert('L'))
#         print(prevImage.shape) 
#         print(prevImage.dtype)
        # Evaluate the difference
        diffImage = sum(absolute(subtract(float32(currImage), float32(prevImage))))/720/1280
#         diffImage = sum(absolute(subtract(float32(currImage), float32(prevImage))))
#         print(diffImage.shape)
#         print(diffImage.dtype)
        print(diffImage)

        # Once motion detection is done, make the prior image the current
        prior_image = current_image
        if diffImage > 20:
            return True
        else:
            return False
        

with picamera.PiCamera() as camera:
    # setup GPIO
    led = LED(13)
    solenoid = LED(19)
    
    # setup camera
    camera.resolution = (1280, 720)
    stream = picamera.PiCameraCircularIO(camera, seconds=10)
    camera.start_recording(stream, format='h264')
    
    try:
        while True:
            # If hour is between 18:00 and 6 AM, turn on LEDs
            ct = datetime.now()
            hr = ct.hour
            if (hr >= 18) or (hr <= 6):
                # turn on power to the LEDs
                led.on()
            else:
                # turn off power to the LEDs
                led.off()
                
            # Start main code
            camera.wait_recording(1)
            if detect_motion(camera):
                print('Motion detected!')
                # As soon as we detect motion, split the recording to
                # record the frames "after" motion
                ct = datetime.now()
                filename = 'after' + ct.strftime("%y%m%d%H%M%S") + '.h264'
                camera.split_recording('/home/pi/ftp/files/' + filename)
                print('split recording to ' + filename)
                # Write the 10 seconds "before" motion to disk as well
                filename = 'before' + ct.strftime("%y%m%d%H%M%S") + '.h264'
                stream.copy_to('/home/pi/ftp/files/' + filename, seconds=10)
                stream.clear()
                print('write prior video to ' + filename)
                # turn on solenoid
                solenoid.on()
                # Wait until motion is no longer detected, then split
                # recording back to the in-memory circular buffer
                camera.wait_recording(2)
                while detect_motion(camera):
                    camera.wait_recording(1)
                print('Motion stopped!')
                camera.split_recording(stream)
                # turn off solenoid
                solenoid.off()
                break # for testing purposes, to run the code only once
                
    finally:
        camera.stop_recording()